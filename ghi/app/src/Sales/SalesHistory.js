import React, {useState, useEffect} from 'react'

function SalesHistory({ sales }) {
    const [salespersons, setSalespersons] = useState([])

    const salespersonData = async () => {
        const url = 'http://localhost:8090/api/salespersons/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setSalespersons(data.sales_persons)
        }
      }

    const [salesperson, setSalesperson] = useState('')
    const salespersonchange = (event) => {
        const value = event.target.value;
        setSalesperson(value)
    }

    useEffect(() => {
        salespersonData();
      }, []);

  return (
    <>
    <div className="my-5 container">
            <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                  <div className="card-body">
                    <form id="create-sale-form">
                    <h1 className="card-title">Sales Person History</h1>

                    <div className="row">
                    <div className="col">
                    <div className="mb-3">
                    <select onChange={salespersonchange} required id="salesperson" name="salesperson" className="form-select" value={salesperson}>
                            <option value="">Choose a sales person</option>
                            {salespersons.map(salespersonss => {
                                return (
                            <option key={salespersonss.id} value={salespersonss.id}>
                            {salespersonss.employee_name}
                            </option>
                    );
                    })}
                    </select>
                    </div>
                        </div>
                        </div>
                        </form>
                         </div>
                </div>
              </div>
             </div>
          </div>
          <table className="table table-striped table-hover align-middle mt-5">
          <thead>
            <tr>
              <th>Sales person</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Sale price</th>


            </tr>
          </thead>
          <tbody>
            {sales.map(sale => {
                if (sale.sales_person.id == salesperson) {
                    return (
                        <tr key={sale.id}>
                            <td>{ sale.sales_person.employee_name }</td>
                            <td>{ sale.customer.customer_name }</td>
                            <td>{ sale.automobile.vin }</td>
                            <td>${ sale.price }</td>
                        </tr>
                    );
                }
            })}
          </tbody>
        </table>
          </>
  )
}

export default SalesHistory
