import React, {useEffect, useState } from 'react'

function SalesPersonForm() {

    const [name, setName] = useState('')
    const Namechange = (event) => {
        const value = event.target.value;
        setName(value)
    }

    const [number, setNumber] = useState('')
    const numberchange = (event) => {
        const value = event.target.value;
        setNumber(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.employee_name = name
        data.employee_number = number


        const salespersonURL = 'http://localhost:8090/api/salespersons/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(salespersonURL, fetchConfig)
        if (response.ok) {
            const newModel = await response.json();
            console.log(newModel);

            setName('');
            setNumber('');
        }
    }

  return (
    <div className="my-5 container">
            <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                  <div className="card-body">
                    <form onSubmit={handleSubmit} id="create-salesperson-form">
                      <h1 className="card-title">Create a sales person</h1>

                      <div className="row">
                        <div className="col">
                          <div className="form-floating mb-3">
                            <input onChange={Namechange}  required placeholder="Name" type="text" id="employee_name" name="employee_name" className="form-control" value={name}/>
                            <label htmlFor="employee_name">Name</label>
                          </div>
                          <div className="form-floating mb-3">
                            <input onChange={numberchange}  required placeholder="Employee Number" type="text" id="employee_number" name="employee_number" className="form-control" value={number}/>
                            <label htmlFor="employee_number">Employee Number</label>
                          </div>
                        </div>


                      </div>
                      <button className="btn btn-lg btn-primary">Create</button>
                    </form>
                    <div className="alert alert-success d-none mb-0" id="success-message">
                      Congratulations! Sales person has been saved.
                    </div>
                  </div>
                </div>
              </div>
             </div>
          </div>
  )
}

export default SalesPersonForm
