import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';

import ManufacturersList from './ManufacturersList';
import ManufacturerForm from './ManufacturerForm';

import VehicleModelsList from './VehicleModelsList';
import VehicleModelForm from './VehicleModelForm';

import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';

import AppointmentsList from './AppointmentsList';
import AppointmentsForm from './AppointmentsForm';
import ServiceHistory from './ServiceHistory';

import TechniciansForm from './TechniciansForm';

import SalesPersonForm from './Sales/SalesPersonForm';
import CustomerForm from './Sales/CustomerForm';
import SaleForm from './Sales/SaleForm';
import Saleslist from './Sales/Saleslist';
import SalesHistory from './Sales/SalesHistory';

import Footer from "./Footer";

import { useState, useEffect } from "react";

function App(props) {
  const [manufacturers, setManufacturers] = useState([])
  const [models, setVehicleModels] = useState([])
  const [automobiles, setAutomobiles] = useState([])
  const [appointments, setAppointments] = useState([])
  const [technicians, setTechnicians] = useState([])
  const [sales, setSales] = useState([])

  const fetchManufacturers = async () => {
    const url = 'http://localhost:8100/api/manufacturers/'
    const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const manufacturers = data.manufacturers
        setManufacturers(manufacturers)
      }
  }

  const fetchVehicleModels = async () => {
    const url = 'http://localhost:8100/api/models/'
    const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const models = data.models
        setVehicleModels(models)
      }
  }


  const fetchAutomobiles = async () => {
    const url = 'http://localhost:8100/api/automobiles/'
    const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const automobiles = data.autos
        setAutomobiles(automobiles)
      }
  }

  const fetchAppointments = async () => {
    const url = 'http://localhost:8080/api/appointments/'
    const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const appointments = data.appointments
        setAppointments(appointments)
      }
  }

  const fetchTechnicians = async () => {
    const url = 'http://localhost:8080/api/technicians/'
    const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const technicians = data.technicians
        setTechnicians(technicians)
      }
  }

  const fetchSales = async () => {
    const url = 'http://localhost:8090/api/sales/'
    const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const sale = data.sales
        setSales(sale)
      }
  }



  useEffect(() => {
    fetchManufacturers();
    fetchVehicleModels();
    fetchAutomobiles();
    fetchAppointments();
    fetchTechnicians();
    fetchSales();

  }, [])






  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>

          <Route path="/" element={<MainPage />} />

          <Route path="manufacturers">
              <Route index element={<ManufacturersList manufacturers={manufacturers} fetchManufacturers={fetchManufacturers}/>} />
              <Route path='new' element={<ManufacturerForm fetchManufacturers={fetchManufacturers}/>} />
            </Route>

            <Route path="models">
              <Route index element={<VehicleModelsList models={models} fetchVehicleModels={fetchVehicleModels}/>} />
              <Route path='new' element={<VehicleModelForm fetchVehicleModels={fetchVehicleModels}/>} />
            </Route>

            <Route path="automobiles">
              <Route index element={<AutomobileList automobiles={automobiles} fetchAutomobiles={fetchAutomobiles}/>} />
              <Route path='new' element={<AutomobileForm fetchAutomobiles={fetchAutomobiles}/>} />
            </Route>

            <Route path="appointments">
              <Route index element={<AppointmentsList appointments={appointments} fetchAppointments={fetchAppointments}/>} />
              <Route path='new' element={<AppointmentsForm fetchAppointments={fetchAppointments}/>} />
              <Route path='vin' element={<ServiceHistory fetchAppointments={fetchAppointments}/>} />
            </Route>

            <Route path="technicians/new" element={<TechniciansForm technicians={technicians} fetchTechnicians={fetchTechnicians}/>} />

            <Route path="salesperson/new" element={<SalesPersonForm />} />
            <Route path="customer/new" element={<CustomerForm />} />

            <Route path="sales">
              <Route index element={<Saleslist sales={sales} fetchSales={fetchSales}/>} />
              <Route path='new' element={<SaleForm fetchSales={fetchSales}/>} />
            </Route>

            <Route path="saleshistory" element={<SalesHistory sales={sales}/>} />



        </Routes>
      </div>
      <Footer />
    </BrowserRouter>
  );
}

export default App;
