import React from 'react';


function ManufacturersList({manufacturers, fetchManufacturers}) {




    return (
        <table className="table table-striped table-hover align-middle mt-5">
          <thead>
            <tr>
              <th>Manufacturer name</th>


            </tr>
          </thead>
          <tbody>
            {manufacturers.map(manufacturer => {
                return (
                    <tr key={manufacturer.id}>
                        <td>{ manufacturer.name }</td>
                    </tr>
                );
            })}
          </tbody>
        </table>

    )
}

export default ManufacturersList;
