import {
  MDBCard,
  MDBContainer,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,
  MDBRow,
  MDBCol,
  MDBCarousel,
  MDBCarouselItem,
} from 'mdb-react-ui-kit';
import { Link } from 'react-router-dom';
import React, {useState, useEffect} from 'react';
import welcome from './images/welcome.png';
import autopia2 from './images/autopia2.jpeg';
import autopia5 from './images/autopia5.jpeg';
import autopia6 from './images/autopia6.jpeg';
import autopia7 from './images/autopia7.jpeg';



function MainPage() {


  return (
    <header className="text-center" style={{ paddingLeft: 0 }}>
      <div className="text-center">
      <MDBContainer>
      <MDBRow className='d-flex justify-content-center'>
      <MDBCol md='6' className='bg-primary mt-5 text-white'>
          <MDBCarousel pause={false} interval={7500}>
            <MDBCarouselItem
                  className='h-75 d-block'
                  itemId={1}
                  src={autopia7}
                  height='75'
                  width='auto'
                  alt='...'
                  >
                  <img src={welcome} className='align-items-start' alt='welcome'/>
                  <h5>Manage your staffing and inventory needs with the Autopia App.</h5>
            </MDBCarouselItem>
            <MDBCarouselItem
                  className='h-75 d-block'
                  itemId={2}
                  src={autopia2}
                  height='75'
                  width='auto'
                  alt='...'
                  >
                  <img src={welcome} className='align-items-start' alt='welcome'/>
                  <h5>Manage your staffing and inventory needs with the Autopia App.</h5>
            </MDBCarouselItem>
            <MDBCarouselItem
                  className='h-75 d-block'
                  itemId={3}
                  src={autopia5}
                  height='75'
                  width='auto'
                  alt='...'
                  >
                  <img src={welcome} className='align-items-start' alt='welcome'/>
                  <h5>Manage your staffing and inventory needs with the Autopia App.</h5>
            </MDBCarouselItem>
            <MDBCarouselItem
                  className='h-75 d-block'
                  itemId={4}
                  src={autopia6}
                  height='75'
                  width='auto'
                  alt='...'
                  >
                  <img src={welcome} className='align-items-start' alt='welcome'/>
                  <h5>Manage your staffing and inventory needs with the Autopia App.</h5>
            </MDBCarouselItem>
          </MDBCarousel>
          </MDBCol>
          </MDBRow>
          </MDBContainer>
      </div>







        <div className="container">
          <br></br>
          <p className="fs-2 fw-bold text-center">Your autoshop just got automated.</p>
          <p className="text-center">
            With thanks to Python, Django, Javascript, React, Docker, MDBootstrap, and more!
          </p>
          <br></br>
        </div>

    </header>
  );
}

export default MainPage;
