import { NavLink, Link } from 'react-router-dom';
import SALES from "./images/SALES.png";
import SERVICES from "./images/SERVICES.png";
import inventory from "./images/inventory.png";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light" style={{ backgroundColor:"black" }}>
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/"><img src="https://i.ibb.co/2d21vjm/autopia-black.png" alt="Autopia logo" width="280px"/></NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">

          <div className="collapse navbar-collapse" id="navbarNavDarkDropdown">
              <ul className="navbar-nav">
                <li className="nav-item dropdown">
                  <a className="nav-link dropdown-toggle" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  <img src={inventory} width="200px" className="img-fluid" alt="inventory Button" />
                  </a>
                  <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                    <li> <Link className="dropdown-item" to="/manufacturers/new">Add New Manufacturer</Link></li>
                    <li><Link className="dropdown-item" to="/manufacturers">Manufacturers List</Link></li>
                    <li> <Link className="dropdown-item" to="/models/new">Add New Vehicle Model</Link></li>
                    <li><Link className="dropdown-item" to="/models">Vehicle Models List</Link></li>
                    <li><Link className="dropdown-item" to="/automobiles/new">Add New Automobile</Link></li>
                    <li><Link className="dropdown-item" to="/automobiles">Automobiles List</Link></li>
                    </ul>
                </li>
              </ul>
          </div>

          <div className="collapse navbar-collapse" id="navbarNavDarkDropdown">
              <ul className="navbar-nav" style={{ color: "white" }}>
                <li className="nav-item dropdown">
                  <a className="nav-link dropdown-toggle" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  <img src={SERVICES} width="170px" className="img-fluid" alt="Services Button" />
                  </a>
                  <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                    <li><Link className="dropdown-item" to="/technicians/new">Enter a technician</Link></li>
                    <li><Link className="dropdown-item" to="/appointments/new">Enter a service appointment</Link></li>
                    <li><Link className="dropdown-item" to="/appointments">View appointments</Link></li>
                    <li><Link className="dropdown-item" to="/appointments/vin">Service History</Link></li>
                    </ul>
                </li>
              </ul>
            </div>

          <div className="collapse navbar-collapse" id="navbarNavDarkDropdown">
              <ul className="navbar-nav">
                <li className="nav-item dropdown">
                  <a className="nav-link dropdown-toggle" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    <img src={SALES} className="img-fluid" alt="Sales Button" width="120px"/>
                  </a>
                  <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">



                    <li><Link className="dropdown-item" to="/salesperson/new">Add sales employee</Link></li>
                    <li><Link className="dropdown-item" to="/customer/new">Add customer</Link></li>
                    <li><Link className="dropdown-item" to="/sales">View all sales</Link></li>
                    <li><Link className="dropdown-item" to="/sales/new">Add sale</Link></li>
                    <li><Link className="dropdown-item" to="/saleshistory">View Sales History</Link></li>
                    </ul>
                </li>
              </ul>
            </div>

          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
