import React from "react";
import { MDBFooter, MDBContainer } from 'mdb-react-ui-kit';
import autopialogo from './images/autopialogo.png';

function Footer() {
  return (
    // <footer
    //   className="text-center text-lg-start"
    //   style={{
    //     backgroundColor: "#000000",
    //     color: "#696969",
    //   }}
    // >
    <MDBFooter className='text-center text-white position-static-bottom' style={{ backgroundColor: '#000000' }}>
      <MDBContainer className='p-4'></MDBContainer>

      <div className="container p-4 pb-0" style={{ color: "#696969" }}>
        <section className="">
          <div className="row">
            <div className="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3">
              <img
                src={autopialogo}
                alt="Cinematch logo"
                width="250px"
              />
            </div>
            <hr className="w-100 clearfix d-md-none" />
            <div className="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
              <p>
                <button
                  style={{
                    color: "#696969",
                    border: "none",
                    background: "none",
                    cursor: "pointer",
                  }}
                >
                  Hack Reactor
                </button>
              </p>
              <p>
                <button
                  style={{
                    color: "#696969",
                    border: "none",
                    background: "none",
                    cursor: "pointer",
                  }}
                >
                  Python
                </button>
              </p>
              <p>
                <button
                  style={{
                    color: "#696969",
                    border: "none",
                    background: "none",
                    cursor: "pointer",
                  }}
                >
                  Django
                </button>
              </p>
              <p>
                <button
                  style={{
                    color: "#696969",
                    border: "none",
                    background: "none",
                    cursor: "pointer",
                  }}
                >
                  Terms of Use
                </button>
              </p>
              <p>
                <button
                  style={{
                    color: "#696969",
                    border: "none",
                    background: "none",
                    cursor: "pointer",
                  }}
                >
                  React.js
                </button>
              </p>
            </div>
            <hr className="w-100 clearfix d-md-none" />
            <div className="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3">
              <p>
                <button
                  style={{
                    color: "#696969",
                    border: "none",
                    background: "none",
                    cursor: "pointer",
                  }}
                >
                  Docker
                </button>
              </p>
              <p>
                <button
                  style={{
                    color: "#696969",
                    border: "none",
                    background: "none",
                    cursor: "pointer",
                  }}
                >
                  JSON
                </button>
              </p>
              <p>
                <button
                  style={{
                    color: "#696969",
                    border: "none",
                    background: "none",
                    cursor: "pointer",
                  }}
                >
                  MD Bootstrap
                </button>
              </p>
              <p>
                <button
                  style={{
                    color: "#696969",
                    border: "none",
                    background: "none",
                    cursor: "pointer",
                  }}
                >
                  Javascript
                </button>
              </p>
              <p>
                <button
                  style={{
                    color: "#696969",
                    border: "none",
                    background: "none",
                    cursor: "pointer",
                  }}
                >
                  HTML/CSS
                </button>
              </p>
            </div>
            <hr className="w-100 clearfix d-md-none" />
            <div className="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
              <h6 className="text-uppercase mb-4 font-weight-bold">Contact</h6>
              <p>
                <i className="fas fa-home mr-3" /> Abbey Wang
              </p>
              <p>
                <i className="fas fa-envelope mr-3" />{" "} abbeywng@gmail.com
              </p>
              <p>
                <i className="fas fa-phone mr-3" /> + 1 PLZ H1R3 M3
              </p>
              <p>
                <i className="fas fa-print mr-3" /> + 1 WERKD HARD 2 B HERE
              </p>
            </div>
          </div>
        </section>
        <hr className="my-3" />
        <section className="p-3 pt-0">
          <div className="row d-flex align-items-center">
            <div className="col-md-7 col-lg-8 text-center text-md-start">
              <div className="p-3">
                © 2023 Copyright: Abagail Wang
              </div>
            </div>
            <div className="col-md-5 col-lg-4 ml-lg-0 text-center text-md-end">
              <a
                href="https://www.linkedin.com/in/abagail-wang/"
                className="btn btn-outline-light btn-floating m-1"
                role="button"
              >
                <i className="fab fa-linkedin" />
              </a>
              <a
                href="https://gitlab.com/abagailwang"
                className="btn btn-outline-light btn-floating m-1"
                role="button"
              >
                <i className="fab fa-gitlab" />
              </a>
              <a
                href="https://www.google.com/"
                className="btn btn-outline-light btn-floating m-1"
                role="button"
              >
                <i className="fab fa-google" />
              </a>

            </div>
          </div>
        </section>
      </div>
      </MDBFooter>
    /* </footer> */

  );
}
export default Footer;
