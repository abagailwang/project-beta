# Generated by Django 4.0.3 on 2023-01-25 22:16

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0003_sales_availablity'),
    ]

    operations = [
        migrations.RenameField(
            model_name='sales',
            old_name='availablity',
            new_name='availability',
        ),
    ]
