# Generated by Django 4.0.3 on 2023-01-25 22:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0002_alter_sales_price'),
    ]

    operations = [
        migrations.AddField(
            model_name='sales',
            name='availablity',
            field=models.BooleanField(default=True),
        ),
    ]
