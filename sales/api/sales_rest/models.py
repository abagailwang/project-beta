from django.db import models
from django.urls import reverse

class SalesPerson(models.Model):
    employee_name = models.CharField(max_length=200)
    employee_number = models.CharField(max_length=200)

    def get_api_url(self):
        return reverse("sales_person", kwargs={"pk": self.id})


class Customer(models.Model):
    customer_name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=200)

    def get_api_url(self):
        return reverse("api_automobile", kwargs={"pk": self.id})

class AutomobileVO(models.Model):
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True)
    availability = models.BooleanField(default = True)

    def get_api_url(self):
        return reverse("api_automobile", kwargs={"vin": self.vin})

class Sales(models.Model):
    sales_person = models.ForeignKey(SalesPerson, related_name='sales_person', on_delete=models.CASCADE,)
    customer = models.ForeignKey(Customer, related_name='customer', on_delete=models.CASCADE,)
    automobile = models.ForeignKey(AutomobileVO, related_name='automobile', on_delete=models.CASCADE,)
    price = models.PositiveIntegerField()
