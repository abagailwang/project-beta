from django.db import models
from django.urls import reverse


class VinVO(models.Model):
    import_href= models.CharField(max_length=300, unique=True, null=True)
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return self.vin

    class Meta:
        verbose_name_plural = "inventory VINs"


class Technician(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveSmallIntegerField()
    def __str__(self):
        return self.name
    def get_api_url(self):
        return reverse("api_create_technician", kwargs={"id": self.id})




class Appointment(models.Model):
    vin = models.CharField(max_length=17)
    customer_name = models.CharField(max_length=200)
    date_time = models.DateTimeField()
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    reason = models.TextField()
    completed = models.BooleanField(default=False, null=True)

    def complete_service(self):
        self.completed = True
        self.save()

    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"pk": self.pk})

    def __str__(self):
        return self.vin
